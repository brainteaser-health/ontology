# BRAINTEASER Ontology

The BRAINTEASER Ontology (BO) jointly models both Amyotrophic Lateral Sclerosis (ALS) and Multiple Sclerosis (MS).
The BO will serve multiple purposes:

* to provide a unified and comprehensive conceptual view about ALS and MS, which are typically dealt with separately, allowing us to coherently integrate the data coming from the different medical partners in the project;
* to seamlessly represent both retrospective data and prospective data, produced during the lifetime of BRAINTEASER;
* to allow for sharing and re-using the BRAINTEASER datasets according to Open Science and FAIR principles.

The BO is innovative since it relies on very few seed concepts - Patient, Clinical Trial, Disease, Event - which allow us to jointly model ALS and MS and to grasp the time dimension entailed by the progression of such diseases.
Indeed, the core idea is that a Patient participates in a Clinical Trial, suffers from some Diseases, and undergoes Events. These Events are different in nature and cover a wide range of cases, e.g. Onset, Pregnancy, Symptom, Trauma, Diagnostic Procedure (like evoked potentials or ALS-FRS questionnaires) Therapeutic Procedure (like Mechanical Ventilation for ALS or Disease-Modifying Therapy for MS), Relapse, and more. Overall, this event-based approach allows us to model ALS and MS in an unified way, sharing concepts among these two diseases, and to track what happens during their progression.

Statistics about the ontology:

* Classes: 379
* Object prop.: 76
* Data prop.: 292
* Individuals: 396
* Nodes: 694
* Edges: 759

All the documentation is available at the [BO website](https://w3id.org/brainteaser/ontology).

### License ###
All the contents of this repository are shared using the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
![CC logo](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

### Acknowledgements ###
This work has been supported by the [BRAINTEASER](https://brainteaser.health/)  project,  funded by the European Union’s Horizon 2020 research and innovation programme under the grant agreement No GA101017598.